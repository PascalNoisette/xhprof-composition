# Profiling php

## Install xhprof on running fpm server

Two target are possible either the inventory inventory/docker or the inventory towars inventory/ssh


```
ansible-playbook xhprof.yml -i inventory/docker -e magento_project_name=trf
```

## Analysis of the traces

The docker image jeromebreton/xhprof-simple-viewer contains the tool to visualize the traces generated above.


You will require docker to launch the tool
```
sudo apt install docker.io
```

```
docker run --rm -p 3731:80 --name jeromebreton-xhprof-simple-viewer -v "`pwd`/xhprof_traces":/traces jeromebreton/xhprof-simple-viewer
```
